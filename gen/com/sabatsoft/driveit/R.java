/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.sabatsoft.driveit;

public final class R {
    public static final class array {
        public static final int units=0x7f050000;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int blue=0x7f060000;
        public static final int grey=0x7f060001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f070000;
        public static final int activity_vertical_margin=0x7f070001;
        public static final int buttonPadding=0x7f070002;
    }
    public static final class drawable {
        public static final int asfalt=0x7f020000;
        public static final int carlogo=0x7f020001;
        public static final int ic_launcher=0x7f020002;
    }
    public static final class id {
        public static final int account_info_fragment_MaxSpeed=0x7f0b000a;
        public static final int account_info_fragment_TotalDistance=0x7f0b0008;
        public static final int account_info_fragment_TotalTime=0x7f0b0009;
        public static final int account_login=0x7f0b0004;
        public static final int account_register=0x7f0b0000;
        public static final int accountinfofragment_AccountName=0x7f0b0007;
        public static final int accountmanager_pager=0x7f0b000b;
        public static final int action_settings=0x7f0b0028;
        public static final int btnLogin=0x7f0b0006;
        public static final int btnRegister=0x7f0b0003;
        public static final int del=0x7f0b0027;
        public static final int del_drive=0x7f0b002a;
        public static final int drive_measurement_avgspeed=0x7f0b0012;
        public static final int drive_measurement_buttonStop=0x7f0b0014;
        public static final int drive_measurement_buttons=0x7f0b0015;
        public static final int drive_measurement_cancelbutton=0x7f0b0016;
        public static final int drive_measurement_date=0x7f0b000e;
        public static final int drive_measurement_distance=0x7f0b000f;
        public static final int drive_measurement_fragment=0x7f0b0018;
        public static final int drive_measurement_maxspeed=0x7f0b0011;
        public static final int drive_measurement_savebutton=0x7f0b0017;
        public static final int drive_measurement_time=0x7f0b0010;
        public static final int drivemeasuring_fragment=0x7f0b000d;
        public static final int gps_status=0x7f0b001e;
        public static final int gps_status_driveActivity=0x7f0b0019;
        public static final int introscreen=0x7f0b001a;
        public static final int introscreenImage=0x7f0b001b;
        public static final int log=0x7f0b0026;
        public static final int login_pass1=0x7f0b0005;
        public static final int mainactivity_pager=0x7f0b001d;
        public static final int modeSpinner=0x7f0b001f;
        public static final int newdrive_startbutton=0x7f0b0022;
        public static final int register_pass1=0x7f0b0001;
        public static final int register_pass2=0x7f0b0002;
        public static final int routeSpinner=0x7f0b0021;
        public static final int show_drive=0x7f0b0029;
        public static final int speedUnit=0x7f0b0025;
        public static final int start_tittle=0x7f0b001c;
        public static final int tachometer=0x7f0b0024;
        public static final int tachometerfragment_startbutton=0x7f0b0023;
        public static final int test=0x7f0b000c;
        public static final int testButton=0x7f0b0013;
        public static final int vehicleSpinner=0x7f0b0020;
    }
    public static final class layout {
        public static final int acc_list_row=0x7f030000;
        public static final int acc_list_view=0x7f030001;
        public static final int account_detail=0x7f030002;
        public static final int account_info_fragment=0x7f030003;
        public static final int accountmanager=0x7f030004;
        public static final int activity_drive_detail=0x7f030005;
        public static final int drive_list_row=0x7f030006;
        public static final int drive_measurement=0x7f030007;
        public static final int driveactivity=0x7f030008;
        public static final int drives_list_view=0x7f030009;
        public static final int dstart=0x7f03000a;
        public static final int mainactivity=0x7f03000b;
        public static final int newdrive=0x7f03000c;
        public static final int tachometer_fragment=0x7f03000d;
    }
    public static final class menu {
        public static final int acc_list_menu=0x7f0a0000;
        public static final int drive_detail=0x7f0a0001;
        public static final int drives_list_menu=0x7f0a0002;
    }
    public static final class string {
        public static final int account_name=0x7f080008;
        public static final int accounts=0x7f080005;
        public static final int action_settings=0x7f080015;
        public static final int app_name=0x7f080000;
        public static final int cancel=0x7f080021;
        public static final int confirm_password=0x7f08000c;
        public static final int delete=0x7f08000f;
        public static final int drives=0x7f08001d;
        public static final int email=0x7f080009;
        public static final int gps_signal_=0x7f08001c;
        public static final int hello_world=0x7f080016;
        public static final int kmph=0x7f080012;
        public static final int login=0x7f080007;
        public static final int logout=0x7f080011;
        public static final int maximum_speed=0x7f08001a;
        public static final int mph=0x7f080013;
        public static final int new_account=0x7f08000b;
        public static final int no_account=0x7f080006;
        public static final int no_drives=0x7f08001e;
        public static final int password=0x7f08000a;
        public static final int refresh=0x7f080001;
        public static final int register=0x7f080017;
        public static final int save_drive=0x7f080020;
        public static final int settings=0x7f080002;
        public static final int show_drive=0x7f08001f;
        public static final int start_new_drive=0x7f08001b;
        public static final int title_activity_accounts_fragment=0x7f080004;
        public static final int title_activity_after_login=0x7f080010;
        public static final int title_activity_daccount=0x7f08000d;
        public static final int title_activity_drive_detail=0x7f080022;
        public static final int title_activity_settings=0x7f080014;
        public static final int title_activity_swiping_test=0x7f08000e;
        public static final int title_activity_test=0x7f080003;
        public static final int total_distance=0x7f080018;
        public static final int total_time=0x7f080019;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.

    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.

        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f090000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f090001;
        public static final int LoginFormContainer=0x7f090002;
    }
    public static final class xml {
        public static final int settings=0x7f040000;
    }
}
