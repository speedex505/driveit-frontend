package com.sabatsoft.driveit.engine;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Drive {
	
	private Date startDate;
	private long duration;
	private float distance;
	/*private Mode mode;
	private Vehicle vehicle;*/
	ArrayList<TrackPoint> trackPoints;
	
	public Drive(Date date){
		this.startDate=date;
		trackPoints=new ArrayList<TrackPoint>();
	}
	public Drive(Date date, long duration, float distance){
		this.startDate=date;
		this.duration=duration;
		this.distance=distance;
		trackPoints = new ArrayList<TrackPoint>();
	}
	
	public void endDrive(long duration, float distance){
		this.duration=duration;
		this.distance=distance;
	}
	
	public void addTrackPoint(TrackPoint trackPoint){
		trackPoints.add(trackPoint);
	}
	public TrackPoint [] getTrackPoints(){
		return trackPoints.toArray(new TrackPoint[0]);		
	}
	public Date getStartDate(){
		return startDate;
	}
	public long getDuration(){
		return duration;
	}
	public float getDistance(){
		return distance;
	}
	@SuppressLint("SimpleDateFormat")
	@Override
	public String toString(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return dateFormat.format(startDate);
	}

	public int getModeID() {
		return 0;
	}
	
	public int getVehicleID(){
		return 0;
	}

	public boolean loadTrackPoints(Context context) {
		FileManager fm = new FileManager(context,String.valueOf(startDate.getTime()));
		try {
			fm.readTrackPoints(this);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public boolean saveTrackPoint(Context context){
		FileManager fm = new FileManager(context,String.valueOf(startDate.getTime()));
		try {
			fm.writeTrackPoints(this);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	public boolean deleteDrive(Context context){
		FileManager fm = new FileManager(context,String.valueOf(startDate.getTime()));
		return fm.deleteDrive();
	}
	
	public void clearTrackPoints() {
		trackPoints.clear();		
	}
}
