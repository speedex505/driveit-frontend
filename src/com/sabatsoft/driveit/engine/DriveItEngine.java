package com.sabatsoft.driveit.engine;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
/*
 * This class contains important static methods and constants
 * 
 */
public class DriveItEngine {
	// Aplication Statuses
	public static final int BEFORE_LOG=0;
	public static final int BEFORE_START=1;
	public static final int MEASURING=2;
	public static final int STOPPED=3;
	// Application status
	private static int status;
	
	// Server name
	public final static String SERVER_NAME = "kus.pod.cvut.cz";
	public final static int SERVER_PORT = 9876;
	// Pattern to recognize email
	private final static Pattern EMAIL_ADDRESS_PATTERN = Pattern
			.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");
	// Constants for unit conversion
	private final static float METER_TO_KILOMETER_PERHOUR=3.6F;
	private final static float METER_TO_MILES_PERHOUR=2.23693629F;
	private final static float METER_TO_KILOMETER=0.001F;
	private final static float METER_TO_MILE=0.000621371192F;
			
	// Shared Settings
	private static final String UNIT = "unit";
	public static int KMPH = 0;
	public static int MPH = 1;
	
	// Logged user
	private static Account currentUser;

	// Drive temp
	private static Drive tmpDrive;
	private static boolean measuring= false;
	
	
	private DriveItEngine() {
	}

	public static int getStatus(){
		return status;
	}
	public static void setStatus(int stat){
		status=stat;
	}
	
	// Hashing method. Account verification
	public static String hashText(String text) {
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
		}
		m.reset();
		m.update(text.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		while(hashtext.length() < 32 ){
		  hashtext = "0"+hashtext;
		}
		return hashtext;
	}
	// Return true if input is valid email
	public static boolean checkEmail(String email) {
		return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
	}
	// Sets current user account into engine variable
	public static void setCurrentAccount(Account currentAccount){
		currentUser = currentAccount;
	}
	
	// Return account of logged user
	public static Account getAccount(){
		return currentUser;
	}
	
	// Convert from meters per hour to UNIT per hour
	public static float meterPerSecondTransfer(float meters, int units){
		if(units==KMPH){
			return meters*METER_TO_KILOMETER_PERHOUR;
		}else {
			return meters*METER_TO_MILES_PERHOUR;
		}
	}
	public static float meterTransfer(float meters, int units){
		if(units==KMPH){
			return meters*METER_TO_KILOMETER;
		}else {
			return meters*METER_TO_MILE;
		}
	}
	
	
	// Return code of UNIT saved in SharedPreferences
    public static int getUnit(Context context){
    	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);  
    	if(preferences.getString(UNIT, "Kilometer").equals("Kilometer")){
    		return KMPH;
    	}else{
    		return MPH;
    	}
    }
    public static boolean isMeasuring(){
    	return measuring;
    }
    public static void measuring(boolean status){
    	measuring=status;
    }
    
    public static void setTmpDrive(Drive drive){
    	tmpDrive=drive;
    }
    public static Drive getTmpDrive(){
    	return tmpDrive;
    }
    
}
