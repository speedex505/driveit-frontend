package com.sabatsoft.driveit.engine;

public class TrackPoint {
	private double latitude;
	private double longitude;
	private long timestamp;
	private float currentSpeed;
	private double accuracy;
	
	public TrackPoint(double latitude,double longitude,long timestamp,float currentSpeed,double accuracy){
		this.latitude=latitude;
		this.longitude=longitude;
		this.timestamp=timestamp;
		this.currentSpeed=currentSpeed;
		this.accuracy=accuracy;
	}
	public TrackPoint(String parsingString){
		String[] parsingUnits = parsingString.split(" ");
		latitude=Double.parseDouble(parsingUnits[1]);
		longitude=Double.parseDouble(parsingUnits[2]);
		timestamp=Long.parseLong(parsingUnits[3]);
		currentSpeed=Float.parseFloat(parsingUnits[4]);
		accuracy=Double.parseDouble(parsingUnits[5]);
	}
	public double getLatitude(){
		return latitude;
	}
	public double getLongitude(){
		return longitude;
	}
	public long getTimeStamp(){
		return timestamp;
	}
	public float getCurrentSpeed(){
		return currentSpeed;
	}
	public double getAccuracy(){
		return accuracy;
	}
	public String getParsingString(){
		String parsing = "TP";
		parsing +=" "+latitude;
		parsing +=" "+longitude;
		parsing +=" "+timestamp;
		parsing +=" "+currentSpeed;
		parsing +=" "+accuracy;
		return parsing;
	}
	
}
