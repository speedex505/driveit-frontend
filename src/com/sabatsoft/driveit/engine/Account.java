package com.sabatsoft.driveit.engine;

import java.util.ArrayList;

public class Account {
	private String account_name;
	private String password;
	private int active;
	private float totalDistance;
	private long totalTime;
	private float maxSpeed;
	private ArrayList<Drive> drives;
	private ArrayList<Mode> modes;
	
	public Account(String account_name, String password, int active) {
		this(account_name,password);
		this.active = active;
	}
	
	public Account(String account_name, String password) {
		this.account_name = account_name;
		this.password = password;
		this.active = 0;
		this.totalDistance=0;
		this.totalTime=0;
		this.maxSpeed=0;
		this.drives= new ArrayList<Drive>();
		this.modes = new ArrayList<Mode>();
		this.modes.add(new Mode("Race",true));
		this.modes.add(new Mode("Casual",false));
	}	
	
	public String getName(){
		return account_name;
	}
	
	public boolean comparePass(String password){
		return(password.equals(this.password));
	}
	
	public boolean isActive(){
		if(active==0)
			return false;
		else
			return true;
	}
	
	public void setTotalDistance(float distance){
		totalDistance=distance;
	}
	
	public float getTotalDistance(){
		return totalDistance;
	}
	
	public void setTotalTime(long time){
		totalTime=time;
	}
	
	public long getTotalTime(){
		return totalTime;
	}
	
	public void updateMaxSpeed(float speed){
		if(maxSpeed<speed){
			maxSpeed=speed;
		}
	}
	
	public float getMaxSpeed(){
		return maxSpeed;
	}
	public void addDrive(Drive drive){
		drives.add(drive);
	}
	
	@Override
	public String toString() {
		return account_name	;
	}

	public Drive[] getDrives() {
		Drive [] drivesArray;
		drivesArray= drives.toArray(new Drive[0]);
		return drivesArray;		
	}

	public void removeDrive(Drive clickedDrive) {
		drives.remove(clickedDrive);		
	}

	public Mode[] getModes() {
		Mode [] modesArray;
		modesArray= modes.toArray(new Mode[0]);
		return modesArray;
	}
}
