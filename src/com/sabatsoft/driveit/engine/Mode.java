package com.sabatsoft.driveit.engine;

public class Mode {
	
	private String name;
	private boolean motionSensors;
	public Mode(String parsingString){
		String [] items = parsingString.split(" ");
		name=items[0];
		motionSensors=Boolean.getBoolean(items[1]);
	}
	public Mode(String name, boolean motionSensors){
		this.name=name;
		this.motionSensors=motionSensors;
	}
	
	@Override
	public String toString() {	
		return name;
	}
	public String generateParseString(){
		String ret="";
		ret+=name;
		ret+=" "+String.valueOf(motionSensors);
		return ret;
	}

}
