package com.sabatsoft.driveit.serverconnection;

public enum Code {
	CLOSE(500), ERROR(505), LISTENING(1000), LOGIN(1001), REGISTER(1002);
	private int code;

	Code(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
