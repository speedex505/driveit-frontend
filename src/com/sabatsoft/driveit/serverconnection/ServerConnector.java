package com.sabatsoft.driveit.serverconnection;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import com.sabatsoft.driveit.engine.DriveItEngine;

public class ServerConnector {
	private Socket socket = null;
	private BufferedInputStream streamIn = null;
	private PrintWriter streamOut = null;

	public boolean loginUser(String username, String password)
			throws IOException {
		String message;
		message = recieveMessage();
		if (!message.equals(String.valueOf(Code.LISTENING.getCode())))
			return false;
		message = Code.LOGIN.getCode() + " " + username + " " + password;
		sendMessage(message);
		message = recieveMessage();
		if (message.equals(String.valueOf(Code.LISTENING.getCode()))) {
			return true;
		}
		return false;
	}

	public boolean registerUser(String username, String password)
			throws IOException {
		String message;
		message = recieveMessage();
		if (!message.equals(String.valueOf(Code.LISTENING.getCode())))
			return false;
		message = Code.REGISTER.getCode() + " " + username + " " + password;
		sendMessage(message);
		message = recieveMessage();
		if (message.equals(String.valueOf(Code.LISTENING.getCode()))) {
			return true;
		}
		return false;
	}

	public void sendMessage(String message) {
		streamOut.print(message + "\r\n");
		streamOut.flush();
	}

	public String recieveMessage() throws IOException {
		StringBuilder b = new StringBuilder(Load(streamIn));
		if ("error".equals(b.toString())) {
			close();
		}
		b.replace(b.length() - 2, b.length(), "");
		return b.toString();
	}

	public void open() throws IOException {
		socket = new Socket(DriveItEngine.SERVER_NAME, DriveItEngine.SERVER_PORT);
		socket.setSoTimeout(5000);
		streamOut = new PrintWriter(socket.getOutputStream(), true);
		streamIn = new BufferedInputStream(socket.getInputStream());
	}

	public void close() {
		if(streamOut!= null&&socket!=null)
			sendMessage(String.valueOf(Code.CLOSE.getCode()));
		if (streamIn != null) {
			try {
				streamIn.close();
			} catch (IOException e) {
			}
		}
		if (streamOut != null) {
			streamOut.close();
		}
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
			}
		}
	}

	private static String Load(BufferedInputStream in) throws IOException {
		int c = 0;
		StringBuilder msg = new StringBuilder();
		while (true) {
			if (c == -1) {
				return "error";
			}
			c = in.read();
			msg.append((char) c);
			if ((char) c == '\n' && msg.charAt(msg.length() - 2) == '\r') {
				break;
			}
		}
		return msg.toString();
	}
}
