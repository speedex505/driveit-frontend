package com.sabatsoft.driveit.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class LocatorService extends Service implements LocationListener{
	private final IBinder binder = new LocatorBinder();
	private Location lastLocation=null;
	private LocationManager locationManager=null;
	private float distance;
	private float speed;
	private float totalDistance=0;
	private long totalTime=0;
	private long startTime=0;
	private double latitude=0;
	private double longitude=0;
	private double accuracy=0;
	
	
	private boolean gpsRunning=false;
	private boolean isMoving=false;
	
	public static final int ERROR = 0;
	public final static int OK=1;
	public final static int LOW_ACCURACY=2;
	public final static int NO_SIGNAL=3;

	Notification notif;
	PendingIntent pIntent;
	NotificationManager notifManager;
	
	@Override
	public void onCreate() {
		super.onCreate();
		notif=new Notification.Builder(this)
        .setContentTitle("DriveIT")
        .setContentText("GPS service is running")
        //.setLargeIcon(android.R.drawable.ic_menu_directions)
        .setSmallIcon(android.R.drawable.ic_menu_directions)
        .setContentIntent(pIntent).build();
		notif.flags|=Notification.FLAG_NO_CLEAR;
		notifManager= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		totalDistance=0;
	}

	@Override
	public IBinder onBind(Intent intent) {
		Bundle bundle=intent.getExtras();
		Float pomTotalDistance=bundle.getFloat("totalDistance",-1);		
		Long pomTotalTime=bundle.getLong("totalTime",-1);
		if(pomTotalDistance!=-1){
			totalDistance=pomTotalDistance;
		}
		if(pomTotalTime!=-1){
			totalTime=pomTotalTime;
		}
		return binder;
	}

	@Override
	public void onDestroy() {
		stopGPS();
		super.onDestroy();
	}
	
	@Override
	public void onLocationChanged(Location location) {
		if(location.getAccuracy()>100){
			sendMessage(NO_SIGNAL);
			lastLocation=null;
			return;
		}
		//low accuracy
		if(location.getAccuracy()>20){
			sendMessage(LOW_ACCURACY);
			return;
		}
		//Time measurement
		if(isMoving){
			long nowTime=System.currentTimeMillis();
			totalTime=(nowTime-startTime)+totalTime;
			startTime=nowTime;
			if(!location.hasSpeed()||location.getSpeed()==0){
				isMoving=false;
			}
		}else{
			if(location.hasSpeed()&&location.getSpeed()>0.276){
				startTime=System.currentTimeMillis();
				isMoving=true;
			}
		}
		if(lastLocation!=null){
			if(location.hasSpeed()==false){
				speed=0.0F;
			}else{
				speed=location.getSpeed();
				if(speed>0.276){
					distance+=location.distanceTo(lastLocation);
					totalDistance+=location.distanceTo(lastLocation);					
				}
			}
		}
		latitude=location.getLatitude();
		longitude=location.getLongitude();
		
		lastLocation=location;
		sendMessage(OK);
		return;
	}
	private void sendMessage(int message) {
	    Intent intent = new Intent("LocatorService");
	    sendLocationBroadcast(intent,message);
	}
	private void sendLocationBroadcast(Intent intent,int message){
	    intent.putExtra("speed", speed);
	    intent.putExtra("distance", distance);
	    intent.putExtra("totalDistance",totalDistance);
	    intent.putExtra("totalTime",totalTime);
	    intent.putExtra("longitude", longitude);
	    intent.putExtra("latitude", latitude);
	    intent.putExtra("accuracy", accuracy);
	    intent.putExtra("message", message);
	    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}	
	@Override
	public void onProviderDisabled(String provider) {		
	}
	@Override
	public void onProviderEnabled(String provider) {	
	}	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {	
	}
	
    public class LocatorBinder extends Binder {
        public LocatorService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocatorService.this;
        }
    }    
	
	public boolean startGPS(){
		if(gpsRunning){
			return false;
		}
		distance = 0.0F;
		speed = 0.0F; 
		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,0, this);
		//locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,2000,10,this);
		gpsRunning=true;
		notifManager.notify(0,notif);
		return true;
	}
	
	public boolean stopGPS(){
		if(!gpsRunning){
			notifManager.cancel(0);
			return false;			
		}
		locationManager.removeUpdates(this);
		gpsRunning=false;
		sendMessage(OK);
		notifManager.cancel(0);
		return true;
	}
	
	public void resetDistance(){
		distance=0.0F;
	}
	
	public boolean isGPSRunning(){
		return gpsRunning;
	}
}
