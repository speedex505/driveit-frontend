package com.sabatsoft.driveit.service;


import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class SensorService extends Service implements SensorEventListener{
	private final IBinder binder = new SensorBinder();
	
	private SensorManager mSensorManager;
	List<Sensor> deviceSensors;
	
	float calibrationX=0F;
	float calibrationY=0F;
	float calibrationZ=0F;
	
	float x;
	float y;
	float z;
	
	@Override
	public void onCreate() {
		super.onCreate();
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		//deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);		
		mSensorManager.registerListener(this,
				mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
		Log.d("test", "serviceCreated");
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		mSensorManager.unregisterListener(this);
	}


	@Override
	public IBinder onBind(Intent arg0) {
		return binder;
	}
	
	
    public class SensorBinder extends Binder {
        public SensorService getService() {
            // Return this instance of LocalService so clients can call public methods
            return SensorService.this;
        }
    }


	@Override
	public void onAccuracyChanged(Sensor sensor, int arg1) {
	}


	@Override
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
			accelerometerData(event);
		}
	}
	
	public void calibrate(){
		calibrationX=(x-calibrationX)*(-1);
		calibrationY=(y-calibrationY)*(-1);
		calibrationZ=(z-calibrationZ)*(-1);
	}

	private void accelerometerData(SensorEvent event) {
		x=event.values[0] + calibrationX;
		y=event.values[1] + calibrationY;
		z=event.values[2] + calibrationZ;
		Intent intent = new Intent("SensorService");
		intent.putExtra("x",x);
		intent.putExtra("y",y);
		intent.putExtra("z",z);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
}