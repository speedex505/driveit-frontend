package com.sabatsoft.driveit.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.engine.DriveItEngine;

public class AccountInfoFragment extends Fragment {
	
	private TextView account;
	private TextView totalDistance;
	private TextView totalTime;
	private TextView maxSpeed;
	private int unit;
	
	@Override
	public void onResume() {
		super.onResume();
		unit = DriveItEngine.getUnit(getActivity());
		UpdateUI();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.account_info_fragment, container,false);
		account = (TextView) view.findViewById(R.id.accountinfofragment_AccountName);
		totalDistance = (TextView) view.findViewById(R.id.account_info_fragment_TotalDistance);
		totalTime = (TextView) view.findViewById(R.id.account_info_fragment_TotalTime);
		maxSpeed = (TextView) view.findViewById(R.id.account_info_fragment_MaxSpeed);
		account.setText(DriveItEngine.getAccount().getName());
		totalDistance.setText("0 km");
		totalTime.setText("0s");
		return view;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (this.isVisible()) {
	        // If we are becoming visible, then...
	        if (isVisibleToUser) {
	        	UpdateUI();	
	        }
	    }
	}

	private void UpdateUI() {
		float totalDist=DriveItEngine.getAccount().getTotalDistance();
		float maxSp= DriveItEngine.getAccount().getMaxSpeed();
		float updatedSpeed = DriveItEngine.meterPerSecondTransfer(maxSp, unit);
		if(unit==DriveItEngine.KMPH){
			totalDistance.setText(String.format("%.2f",(totalDist/1000))+" km");
			maxSpeed.setText(String.format("%.1f", updatedSpeed)+" kmph");
		}else{
			totalDistance.setText(String.format("%.2f",(totalDist/1609.344))+" mi");
			maxSpeed.setText(String.format("%.1f", updatedSpeed)+" mph");
		}
		long totalT=DriveItEngine.getAccount().getTotalTime()/1000;
		long totalSec=totalT%60;
		long totalMin=totalT/60;
		if(totalMin>0){
			totalTime.setText(totalMin+"m "+totalSec+"s");
		}else{
			totalTime.setText(totalSec+"s");
		}
	}
}
