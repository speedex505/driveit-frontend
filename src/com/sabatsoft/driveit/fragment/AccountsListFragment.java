package com.sabatsoft.driveit.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.activity.MainActivity;
import com.sabatsoft.driveit.database.DBConnector;
import com.sabatsoft.driveit.engine.Account;
import com.sabatsoft.driveit.engine.DriveItEngine;

public class AccountsListFragment extends ListFragment {
	private Account [] accounts;	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		registerForContextMenu(getListView());
		displayListView();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.acc_list_view, container, false);
		return view;
	}

	private void displayListView() {

		DBConnector db = new DBConnector(getActivity());
		db.open();
		accounts = db.getAccounts();
		db.close();

		// create an ArrayAdaptar from the String Array
		ArrayAdapter<Account> dataAdapter = new ArrayAdapter<Account>(
				getActivity(), R.layout.acc_list_row, accounts);
		final ListView listView = (ListView) getView()
				.findViewById(android.R.id.list);

		// Assign adapter to ListView
		listView.setAdapter(dataAdapter);

		// enables filtering for the contents of the given ListView
		listView.setTextFilterEnabled(true);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				listView.showContextMenuForChild(view);
			}
		});
	}
    /** This will be invoked when an item in the listview is long pressed */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.acc_list_menu , menu);
        ListView list = (ListView)v;
        AdapterContextMenuInfo info = (AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle("Account:\n"+list.getItemAtPosition(info.position).toString());
    }


    public boolean onContextItemSelected(MenuItem item) {
 
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        Account currentAccount = accounts[info.position];
        switch(item.getItemId()){
            case R.id.log:
                loginAccount(currentAccount);            	
                break;
            case R.id.del:
            	deleteAccount(currentAccount);                
                break;
        }
        return true;
    }

	private void deleteAccount(Account currentAccount) {
		final Account account=currentAccount;

		new AlertDialog.Builder(getActivity())
	    .setTitle("Delete account")
	    .setMessage("Account: "+currentAccount+"\nDo you really want to delete?")
	    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        	DBConnector db = new DBConnector(getActivity());
	        	db.open();
	        	db.deleteAccount(account.toString());
	        	db.close();
	        	Toast.makeText(getActivity(), "Account deleted", Toast.LENGTH_SHORT).show();
	        	displayListView();
	        }
	    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            // Do nothing
	        }
	    }).show();

	}

	private void loginAccount(Account currentAccount) {
		final Account acc = currentAccount;
		final EditText input = new EditText(getActivity());
		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		new AlertDialog.Builder(getActivity())
	    .setTitle("Sign in")
	    .setMessage("Account: "+currentAccount+"\nPassword:")
	    .setView(input)
	    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            DBConnector db =new DBConnector(getActivity());
	            db.open();
	            String password = DriveItEngine.hashText(input.getText().toString());
	            if(db.loginAccount(acc.getName(), password)){
	            	db.close();
	            	login(acc);
	            }
	            else{
	            	db.close();
	            	Toast.makeText(getActivity(), "Invalid password", Toast.LENGTH_SHORT).show();
	            }
	        }


	    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        	//Do nothing
	        }
	    }).show();
	}
	private void login(Account acc) {
		DriveItEngine.setCurrentAccount(acc);
		DBConnector db = new DBConnector(getActivity());
		db.open();
		db.refreshActiveAccounts(acc.getName());
		db.close();
		DriveItEngine.setStatus(DriveItEngine.BEFORE_START);
		Intent test= new Intent(getActivity(),MainActivity.class);
		getActivity().startActivity(test);
		getActivity().finish();	
	}
}