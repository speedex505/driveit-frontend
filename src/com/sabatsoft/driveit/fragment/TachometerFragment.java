package com.sabatsoft.driveit.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.activity.MainActivity;
import com.sabatsoft.driveit.engine.DriveItEngine;
import com.sabatsoft.driveit.service.LocatorService;

public class TachometerFragment extends Fragment {
	private int unit = 0;
	private TextView speedmeter;
	private TextView speedUnit;
	private Button button;
	private float speed;
	private float totalDistance;
	private long totalTime;
	
	private ViewPager mViewPager;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewPager=MainActivity.mViewPager;
		speed = 0.0F;
		totalDistance=DriveItEngine.getAccount().getTotalDistance();
		totalTime=DriveItEngine.getAccount().getTotalTime();
		View view = inflater.inflate(R.layout.tachometer_fragment, container,false);
		speedmeter = (TextView) view.findViewById(R.id.tachometer);
		speedUnit = (TextView) view.findViewById(R.id.speedUnit);
		button = (Button)view.findViewById(R.id.tachometerfragment_startbutton);
		if(DriveItEngine.getStatus()==DriveItEngine.BEFORE_START){
			button.setText("Start new drive");
		}else{
			button.setText("Stop drive");
		}
		
		Typeface tachometerFont = Typeface.createFromAsset(getActivity()
				.getAssets(), "fonts/ds-digi.ttf");
		speedmeter.setTypeface(tachometerFont);
		speedmeter.setText("--");
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mMessageReceiver, new IntentFilter("LocatorService"));		
		button.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				mViewPager.setCurrentItem(2); 
			}
						
		});
		
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		unit = DriveItEngine.getUnit(getActivity());
		if (DriveItEngine.getUnit(getActivity()) == 0) {
			speedUnit.setText("kmph");
		} else {
			speedUnit.setText("mph");
		}
	}

	private void updateUI() {
		int updateSpeed = (int) DriveItEngine.meterPerSecondTransfer(speed, unit);
		speedmeter.setText(String.format("%02d", updateSpeed));
	}

	private void lowAccuracy() {
		speedmeter.setText("--");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
	}
	
	private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int message = intent.getIntExtra("message", LocatorService.ERROR);
			if (message == LocatorService.LOW_ACCURACY||message == LocatorService.NO_SIGNAL) {
				lowAccuracy();
				return;
			}
			speed = intent.getFloatExtra("speed", 0);
			totalDistance = intent.getFloatExtra("totalDistance", 0);
			totalTime= intent.getLongExtra("totalTime", 0);
			DriveItEngine.getAccount().setTotalDistance(totalDistance);
			DriveItEngine.getAccount().setTotalTime(totalTime);
			DriveItEngine.getAccount().updateMaxSpeed(speed);
			updateUI();
		}
	};


}
