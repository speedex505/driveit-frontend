package com.sabatsoft.driveit.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.activity.DriveActivity;
import com.sabatsoft.driveit.engine.Account;
import com.sabatsoft.driveit.engine.DriveItEngine;
import com.sabatsoft.driveit.engine.Mode;

public class NewDriveFragment extends Fragment {
	private Button start;
	private Spinner modeSpinner;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.newdrive, container, false);
		start= (Button) view.findViewById(R.id.newdrive_startbutton);
		modeSpinner = (Spinner) view.findViewById(R.id.modeSpinner);
		fillSpinners();
		addSpinnersListeners();
		addButtonListeners();
		return view;
	}
	private void addSpinnersListeners() {	
	}
	private void fillSpinners() {
		Account acc = DriveItEngine.getAccount();
		Mode[] modes =acc.getModes();
		List<String> list = new ArrayList<String>();
		for(int i=0;i<modes.length;i++){
			list.add(modes[i].toString());
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		modeSpinner.setAdapter(dataAdapter);
		
	}
	private void addButtonListeners() {
		start.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view){
				Intent intent = new Intent(getActivity(),DriveActivity.class);
				getActivity().startActivity(intent);
			}
		});
	}

}
