package com.sabatsoft.driveit.fragment;

import java.util.Date;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.database.DBConnector;
import com.sabatsoft.driveit.engine.Drive;
import com.sabatsoft.driveit.engine.DriveItEngine;
import com.sabatsoft.driveit.engine.TrackPoint;
import com.sabatsoft.driveit.service.LocatorService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DriveMeasurement extends Fragment{

	private Button cancel;
	private Button save;
	private Button stop;
	
	private LinearLayout buttonBar;
	private TextView driveTime;
	private TextView driveDate;
	private TextView driveMaxSpeed;
	private TextView driveDistance;
	
	private TextView test;
	
	private long time;
	private float currentSpeed;
	private float maxSpeed;
	private float distance;
	private Drive myDrive=null;
	private int trackpoints=0;
	
	private int unit=DriveItEngine.KMPH;
	boolean recieving = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(savedInstanceState!=null){
			time=savedInstanceState.getLong("time",0);
			maxSpeed=savedInstanceState.getFloat("maxSpeed",0F);
			distance=savedInstanceState.getFloat("distance",0F);
		}
		View view = inflater.inflate(R.layout.drive_measurement, container, false);
		driveTime = (TextView) view.findViewById(R.id.drive_measurement_time);
		driveDate = (TextView) view.findViewById(R.id.drive_measurement_date);
		driveMaxSpeed = (TextView) view.findViewById(R.id.drive_measurement_maxspeed);
		driveDistance = (TextView) view.findViewById(R.id.drive_measurement_distance);
		buttonBar = (LinearLayout) view.findViewById(R.id.drive_measurement_buttons);
		cancel = (Button) view.findViewById(R.id.drive_measurement_cancelbutton);
		stop = (Button) view.findViewById(R.id.drive_measurement_buttonStop);
		save = (Button) view.findViewById(R.id.drive_measurement_savebutton);
		
		test = (TextView) view.findViewById(R.id.test);
	
		addButtonListeners();
		Start();
		updateGUI();
		return view;
	}
	private void addButtonListeners() {
		stop.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view) {
				LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
				recieving=false;
				driveTime.removeCallbacks(timer);
				myDrive.endDrive(time, distance);
				DriveItEngine.setStatus(DriveItEngine.STOPPED);
				updateGUI();
			}			
		});
		save.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view){
				DriveItEngine.getAccount().addDrive(myDrive);
				
				DBConnector db = new DBConnector(getActivity());
				db.open();
				db.insertDrive(DriveItEngine.getAccount().getName(), myDrive);
				db.close();

				myDrive.saveTrackPoint(getActivity());
				
				myDrive=null;
				DriveItEngine.setStatus(DriveItEngine.BEFORE_START);
				getActivity().finish();
			}
		});
		cancel.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view) {
				DriveItEngine.setStatus(DriveItEngine.BEFORE_START);
				getActivity().finish();
			}			
		});
	}
	
	private void updateGUI(){
		switch(DriveItEngine.getStatus()){
		case DriveItEngine.STOPPED:
			buttonBar.setVisibility(View.VISIBLE);
			stop.setVisibility(View.GONE);
			break;
		case DriveItEngine.BEFORE_START:
			Toast.makeText(getActivity(), "BEFORESTART STATUS ERROR", Toast.LENGTH_SHORT).show();
			break;
		case DriveItEngine.MEASURING:
			buttonBar.setVisibility(View.GONE);
			stop.setVisibility(View.VISIBLE);
			break;			
		}
		driveTime.setText(formatTime());
		String distanceStr=String.format("%.2f", DriveItEngine.meterTransfer(distance, unit));
		String maxSpeedStr=String.format("%.1f", DriveItEngine.meterPerSecondTransfer(maxSpeed, unit));
		if(unit==DriveItEngine.KMPH){
			driveDistance.setText(distanceStr+" km");
			driveMaxSpeed.setText(maxSpeedStr+" kmph");
		}else{
			driveDistance.setText(distanceStr+" miles");
			driveMaxSpeed.setText(maxSpeedStr+" mph");
		}
		test.setText("Trackpoints: "+trackpoints);
		}
	public void Start(){
		myDrive=startDrive();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mMessageReceiver, new IntentFilter("LocatorService"));
		recieving=true;
		DriveItEngine.setStatus(DriveItEngine.MEASURING);
	}
	private Drive startDrive() {
		Date date = new Date();
		Drive drive = new Drive(date);
		driveTime.postDelayed(timer, 1000);
		driveDate.setText(drive.toString());
		return drive;
	}
	
	Runnable timer = new Runnable () {
	    @Override public void run() {
	        time++;
	        driveTime.setText(formatTime());
	        driveTime.postDelayed(timer, 1000);
	    }
	};
	private CharSequence formatTime() {
		int seconds = (int) (time%60);
		int minutes = (int) (time/60);
		int hours = (int)(time/3600);
		return "Time: "+String.format("%02d", hours)+":"
				+String.format("%02d", minutes)+":"
				+String.format("%02d", seconds);
	}
	
	private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int message = intent.getIntExtra("message", LocatorService.ERROR);
			if (message == LocatorService.LOW_ACCURACY||message == LocatorService.NO_SIGNAL) {
				return;
			}
			currentSpeed = intent.getFloatExtra("speed", 0);
			if(currentSpeed>=maxSpeed){
				maxSpeed=currentSpeed;
			}
			distance = intent.getFloatExtra("distance", 0);
			double latitude = intent.getDoubleExtra("latitude", 0);
			double longitude = intent.getDoubleExtra("longitude", 0);
			double accuracy = intent.getDoubleExtra("accuracy", 0);
			myDrive.addTrackPoint(new TrackPoint(latitude,longitude,System.currentTimeMillis(),currentSpeed,accuracy));
			trackpoints++;
			updateGUI();
		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(recieving){
			LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
		}
	}
}
