package com.sabatsoft.driveit.fragment;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.activity.DriveDetailActivity;
import com.sabatsoft.driveit.database.DBConnector;
import com.sabatsoft.driveit.engine.Drive;
import com.sabatsoft.driveit.engine.DriveItEngine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class DrivesFragment extends ListFragment {
	Drive [] drives;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.drives_list_view, container, false);
		return view;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		registerForContextMenu(getListView());
	}
	private void displayListView() {
		DBConnector db = new DBConnector(getActivity());
		db.open();		
		drives = db.getDrives(DriveItEngine.getAccount().getName());
		db.close();
		// create an ArrayAdaptar from the String Array
		ArrayAdapter<Drive> dataAdapter = new ArrayAdapter<Drive>(
				getActivity(), R.layout.drive_list_row, drives);
		final ListView listView = (ListView) getView()
				.findViewById(android.R.id.list);
		// Assign adapter to ListView
		listView.setAdapter(dataAdapter);
		// enables filtering for the contents of the given ListView
		listView.setTextFilterEnabled(true);
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    if (this.isVisible()) {
	        if (isVisibleToUser) {
	        	displayListView();
	        }
	    }
	}
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.drives_list_menu , menu);
    }


    public boolean onContextItemSelected(MenuItem item) {
 
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        Drive selectedDrive = drives[info.position];
        switch(item.getItemId()){
            case R.id.show_drive:
            	Intent intent = new Intent(getActivity(),DriveDetailActivity.class);
            	DriveItEngine.setTmpDrive(selectedDrive);
            	getActivity().startActivity(intent);
                break;
            case R.id.del_drive:
            	deleteDrive(selectedDrive);
                break;
        }
        return true;
    }

	private void deleteDrive(Drive selectedDrive) {
		DBConnector db = new DBConnector(getActivity());
		db.open();
		db.deleteDrive(DriveItEngine.getAccount().getName(), selectedDrive.getStartDate());
		db.close();
		
		DriveItEngine.getAccount().removeDrive(selectedDrive);
		
		selectedDrive.deleteDrive(getActivity());
		displayListView();		
	}	
}
