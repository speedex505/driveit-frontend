	package com.sabatsoft.driveit.fragment;

import java.io.IOException;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.activity.MainActivity;
import com.sabatsoft.driveit.database.DBConnector;
import com.sabatsoft.driveit.engine.Account;
import com.sabatsoft.driveit.engine.DriveItEngine;
import com.sabatsoft.driveit.serverconnection.ServerConnector;

public class AccountLoginFragment extends Fragment {
	public static final String EXTRA_TITLE = "title";
	private EditText accLogin;
	private EditText accRegister;
	private EditText pass1Login;
	private EditText pass1Register;
	private EditText pass2Register;
	private ProgressDialog pd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {		
		View view = inflater.inflate(R.layout.account_detail, container, false);
		accLogin = (EditText) view.findViewById(R.id.account_login);
		accRegister = (EditText) view.findViewById(R.id.account_register);
		pass1Login = (EditText) view.findViewById(R.id.login_pass1);
		pass1Register = (EditText) view.findViewById(R.id.register_pass1);
		pass2Register = (EditText) view.findViewById(R.id.register_pass2);
		Button login = (Button) view.findViewById(R.id.btnLogin);
		Button register = (Button) view.findViewById(R.id.btnRegister);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/321impact.ttf");
		login.setTypeface(tf);
		register.setTypeface(tf);
		register.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				registerBtnClick(v);
			}
		});
		login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loginBtnClick(v);
			}
		});
		return view;
	}
	
	public void registerBtnClick(View v) {
		final String username = accRegister.getText().toString()
							.toLowerCase(Locale.ENGLISH);
		if (!DriveItEngine.checkEmail(username)) {
			Toast.makeText(getActivity(), "Invalid e-mail address",	Toast.LENGTH_SHORT).show();
			clearForm();
			return;
		}
		
		String password1 = pass1Register.getText().toString();
		String password2 = pass2Register.getText().toString();
		if (password1.length() <= 5) {
			Toast.makeText(getActivity(), "Password is too short",Toast.LENGTH_SHORT).show();
			pass1Register.setText("");
			pass2Register.setText("");
			return;
		}
		if (!password1.equals(password2)) {
			Toast.makeText(getActivity(), "Passwords do not match",Toast.LENGTH_SHORT).show();
			pass1Register.setText("");
			pass2Register.setText("");
			return;
		}
		final String password = DriveItEngine.hashText(password1);
		pd = new ProgressDialog(getActivity());
		pd.setTitle("Registartion");
		pd.setMessage("Connecting to server...");
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();
		//Toast.makeText(getActivity(), "Please wait, connecting to server.", Toast.LENGTH_SHORT).show();
		Thread networking = new Thread(new Runnable() {
            public void run() {
        		ServerConnector sc = new ServerConnector();
        		try {
        			sc.open();
        		} catch (IOException e) {
        			threadMsg("Cannot connect to server");
        			pd.dismiss();
        			sc.close();
        			return;
        		}
        		try {
        			if (sc.registerUser(username, password)) {
        				threadMsg("Registration succeeded");
        			} else {
        				threadMsg("Registration failed");
        			}
        		} catch (IOException e) {
        			pd.dismiss();
        		}
        		pd.dismiss();
        		sc.close();        		
            }

            private void threadMsg(String msg) {
                if (!msg.equals(null) && !msg.equals("")) {
                    Message msgObj = handler.obtainMessage();
                    Bundle b = new Bundle();
                    b.putString("message", msg);
                    msgObj.setData(b);
                    handler.sendMessage(msgObj);
                }
            }

            // Define the Handler that receives messages from the thread and update the progress
            private final Handler handler = new Handler() {
                public void handleMessage(Message msg) {                     
                    String aResponse = msg.getData().getString("message");
                    Toast.makeText(getActivity(), aResponse, Toast.LENGTH_SHORT).show();
                    if(aResponse.equals("Registration succeeded")){
						DBConnector db = new DBConnector(getActivity());
						db.open();
						db.insertAccount(username, password);
						db.close();
                    	login(new Account(username,password));
                    }
        			clearForm();
                }
            };
        });
		networking.start();
	}

	public void loginBtnClick(View v) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				DBConnector db = new DBConnector(getActivity());
				final String username = accLogin.getText().toString().toLowerCase(Locale.ENGLISH);
				final String password = DriveItEngine.hashText(pass1Login.getText().toString());
				if (!DriveItEngine.checkEmail(username)) {
					Toast.makeText(getActivity(), "Invalid e-mail",Toast.LENGTH_SHORT).show();
					clearForm();
					return;
				}
				if (password.length() <= 5) {
					Toast.makeText(getActivity(), "Password is too short",Toast.LENGTH_SHORT).show();
					pass1Login.setText("");
					return;
				}
				db.open();
				if(db.loginAccount(username, password)){
					db.close();
					login(new Account(username,password));
					return;
				}
				db.close();
				Toast.makeText(getActivity(), "Please wait, connecting to server.", Toast.LENGTH_SHORT).show();
				Thread networking = new Thread(new Runnable() {
		            public void run() {
						ServerConnector sc = new ServerConnector();
						try {
							sc.open();
						} catch (IOException e) {
							threadMsg("Cannot connect to server");
							sc.close();
							return;
						}
						try {
							if (sc.loginUser(username, password)) {
								threadMsg("Login succeeded");
								DBConnector db = new DBConnector(getActivity());
								db.open();
								db.insertAccount(username, password);
								db.close();

								return;
							} else {
								threadMsg("Login failed");
							}
						} catch (IOException e) {
							threadMsg("ServerConnector fail");
						}
						sc.close();
		            }

		            private void threadMsg(String msg) {
		                if (!msg.equals(null) && !msg.equals("")) {
		                    Message msgObj = handler.obtainMessage();
		                    Bundle b = new Bundle();
		                    b.putString("message", msg);
		                    msgObj.setData(b);
		                    handler.sendMessage(msgObj);
		                }
		            }

		            // Define the Handler that receives messages from the thread and update the progress
		            private final Handler handler = new Handler() {
		                public void handleMessage(Message msg) {                     
		                    String aResponse = msg.getData().getString("message");
		                    Toast.makeText(getActivity(), aResponse, Toast.LENGTH_SHORT).show();
		                    if(aResponse.equals("Login succeeded")){
		                    	login(new Account(username,password));
		                    }
		        			clearForm();
		                }
		            };
		        });
				networking.start();
			}
		});
	}

	public static Bundle createBundle(String title) {
		Bundle bundle = new Bundle();
		bundle.putString(EXTRA_TITLE, title);
		return bundle;
	}
	private void login(Account acc) {
		DriveItEngine.setCurrentAccount(acc);
		DBConnector db = new DBConnector(getActivity());
		db.open();
		db.refreshActiveAccounts(acc.getName());
		db.close();
		DriveItEngine.setStatus(DriveItEngine.BEFORE_START);
		Intent test= new Intent(getActivity(),MainActivity.class);
		getActivity().startActivity(test);
		getActivity().finish();
	}
	private void clearForm() {
		accLogin.setText("");
		accRegister.setText("");
		pass1Login.setText("");
		pass1Register.setText("");
		pass2Register.setText("");

	}
}
