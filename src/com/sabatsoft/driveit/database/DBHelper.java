package com.sabatsoft.driveit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class DBHelper extends SQLiteOpenHelper{
	
	protected static final String DATABASE_NAME = "driveit.db";
    protected static final int DATABASE_VERSION = 1;

    /** table accounts */
    public static final String TABLE_ACCOUNTS 		= "accounts";
    public static final String COLUMN_ACCOUNT_NAME 	= "account";
    public static final String COLUMN_PASSWORD 		= "password";
    public static final String COLUMN_ACTIVE 		= "active";    
    public static final String COLUMN_TOTAL_DISTANCE="totalDistance";
    public static final String COLUMN_TOTAL_TIME	="totalTime";
    public static final String COLUMN_MAX_SPEED		="maxSpeed";
    
    /** table mode */
    public static final String TABLE_MODE 			= "mode";
    public static final String COLUMN_MODE_ID 		= "idMode";
    public static final String COLUMN_MODE_NAME 	= "modeName";
    public static final String COLUMN_MOTIONSENSORS = "motionSensors";
    //public static final String FOREIGNKEY_ACCOUNT = "fKeyAccount";
    
    /** table vehicle */
    public static final String TABLE_VEHICLE		= "vehicle";
    public static final String COLUMN_VEHICLE_ID	= "idVehicle";
    public static final String COLUMN_VEHICLE_NAME	= "vehicleName";
    public static final String COLUMN_VEHICLE_TYPE	= "vehicleType";
    //public static final String FOREIGNKEY_ACCOUNT = "fKeyAccount"
    
    /** table drive */
    public static final String TABLE_DRIVE 			= "drive";
    public static final String COLUMN_DRIVE_ID 		= "idDrive";
    public static final String COLUMN_STARTDATE		= "startDate";
    public static final String COLUMN_DISTANCE 		= "distance";
    public static final String COLUMN_DURATION 		= "duration";
    public static final String FOREIGNKEY_ACCOUNT 	= "fKeyAccount";
    public static final String FOREIGNKEY_MODE		= "fKeyMode";
    public static final String FOREIGNKEY_VEHICLE 	= "fKeyVehicle";
    
    
    /** table trackpoint */
    public static final String TABLE_TRACKPOINT 	= "trackPoint";
    public static final String COLUMN_TRACKPOINT_ID = "idTrackPoint";
    public static final String COLUMN_LONGITUDE		= "longitude";
    public static final String COLUMN_LATITUDE		= "latutide";
    public static final String COLUMN_CURRENT_SPEED = "currentSpeed";
    public static final String COLUMN_TIMESTAMP		= "timestamp";
    public static final String COLUMN_ACCURACY		= "accuracy";
    public static final String FOREIGNKEY_DRIVE		= "fKeyDrive";
    
    
	public DBHelper(Context context) {
		super(context,DATABASE_NAME, null, DATABASE_VERSION);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		insertAccount(db);
        insertMode(db);
        insertVehicle(db);
        insertDrive(db);
        insertTrackPoint(db);
	}		
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNTS);
        onCreate(db);		
	}
	
	public void insertAccount(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + TABLE_ACCOUNTS + " ("
                + COLUMN_ACCOUNT_NAME 	+ " TEXT PRIMARY KEY ,"
                + COLUMN_PASSWORD 		+ " TEXT NOT NULL,"
                + COLUMN_TOTAL_DISTANCE + " INTEGER ,"
        		+ COLUMN_TOTAL_TIME 	+ " INTEGER ," 
        		+ COLUMN_MAX_SPEED		+ " REAL ,"
                + COLUMN_ACTIVE 		+ " INTEGER NOT NULL"
                + ");");
	}
	
	public void insertMode(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + TABLE_MODE + " ("
                + COLUMN_MODE_ID 		+ " INTEGER PRIMARY KEY ,"
                + COLUMN_MODE_NAME 		+ " TEXT NOT NULL ,"
                + COLUMN_MOTIONSENSORS	+ " INTEGER"
                + ");");
        
        ContentValues modeRace = new ContentValues();
        modeRace.put(COLUMN_MODE_NAME, "Race");
        modeRace.put(COLUMN_MOTIONSENSORS, 1);
        ContentValues modeCasual = new ContentValues();
        modeCasual.put(COLUMN_MODE_NAME, "Casual");
        modeCasual.put(COLUMN_MOTIONSENSORS, 0);
		db.insert(TABLE_MODE, null, modeRace );
		db.insert(TABLE_MODE, null, modeCasual);
	}
	
	public void insertVehicle(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + TABLE_VEHICLE+ " ("
                + COLUMN_VEHICLE_ID 	+ " INTEGER PRIMARY KEY ,"
                + COLUMN_VEHICLE_NAME	+ " TEXT NOT NULL,"
                + COLUMN_VEHICLE_TYPE 	+ " INTEGER NOT NULL "
                + ");");
	}
	public void insertDrive(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + TABLE_DRIVE+ " ("
                + COLUMN_DRIVE_ID	 	+ " INTEGER PRIMARY KEY ,"
                + COLUMN_STARTDATE		+ " INTEGER NOT NULL ,"
                + COLUMN_DISTANCE	 	+ " REAL NOT NULL ,"
                + COLUMN_DURATION		+ " INTEGER NOT NULL ,"
                + FOREIGNKEY_ACCOUNT	+ " TEXT NOT NULL ,"
                + FOREIGNKEY_MODE		+ " INTEGER ,"
                + FOREIGNKEY_VEHICLE	+ " INTEGER"
                + ");");
	}
	public void insertTrackPoint(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + TABLE_TRACKPOINT+ " ("
                + COLUMN_TRACKPOINT_ID 	+ " INTEGER PRIMARY KEY ,"
                + COLUMN_LONGITUDE		+ " REAL NOT NULL ,"
                + COLUMN_LATITUDE	 	+ " REAL NOT NULL ,"
                + COLUMN_CURRENT_SPEED	+ " REAL NOT NULL ,"
                + COLUMN_TIMESTAMP		+ " INTEGER NOT NULL ,"
                + COLUMN_ACCURACY		+ " REAL NOT NULL ,"
                + FOREIGNKEY_DRIVE		+ " INTEGER NOT NULL"
                + ");");
	}
	
}