package com.sabatsoft.driveit.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sabatsoft.driveit.engine.Account;
import com.sabatsoft.driveit.engine.Drive;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DBConnector {

	private DBHelper dbHelper;
	private SQLiteDatabase db;

	public DBConnector(Context context) {
		dbHelper = new DBHelper(context);
	}

	/** Get writeable database */
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	/** Get all accounts cursor */
	private Cursor getAccCursor() {
		Cursor results = db.query(DBHelper.TABLE_ACCOUNTS, null, null, null,
				null, null, DBHelper.COLUMN_ACCOUNT_NAME + " DESC");
		return results;
	}

	/** Returns array of accounts */
	public Account[] getAccounts() {
		List<Account> accounts = new ArrayList<Account>();
		Cursor cursor = getAccCursor();

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Account account = cursorToAccount(cursor);
			accounts.add(account);
			cursor.moveToNext();
		}
		cursor.close();
		return accounts.toArray(new Account[accounts.size()]);
	}

	/** Transform cursor into account */
	private Account cursorToAccount(Cursor cursor) {
		String accname = cursor.getString(cursor
				.getColumnIndex(DBHelper.COLUMN_ACCOUNT_NAME));
		String password = cursor.getString(cursor
				.getColumnIndex(DBHelper.COLUMN_PASSWORD));
		int active = cursor.getInt(cursor
				.getColumnIndex(DBHelper.COLUMN_ACTIVE));
		return new Account(accname, password, active);
	}

	/** Inserts account */
	public boolean insertAccount(String account, String password) {
		ContentValues userValues = new ContentValues();
		userValues.put(DBHelper.COLUMN_ACCOUNT_NAME, account);
		userValues.put(DBHelper.COLUMN_TOTAL_DISTANCE, 0);
		userValues.put(DBHelper.COLUMN_TOTAL_TIME, 0);
		userValues.put(DBHelper.COLUMN_PASSWORD, password);
		userValues.put(DBHelper.COLUMN_ACTIVE, 0);
		if (db.insert(DBHelper.TABLE_ACCOUNTS, null, userValues) == -1) {
			return false;
		}
		return true;
	}

	/** Sets 1 to input account as active account and 0 to other */
	public void refreshActiveAccounts(String account) {
		String args[] = { account };
		ContentValues valuesOther = new ContentValues();
		valuesOther.put(DBHelper.COLUMN_ACTIVE, 0);
		ContentValues valuesAccount = new ContentValues();
		valuesAccount.put(DBHelper.COLUMN_ACTIVE, 1);
		db.update(DBHelper.TABLE_ACCOUNTS, valuesOther, "account<>?", args);
		db.update(DBHelper.TABLE_ACCOUNTS, valuesAccount, "account=?", args);
	}

	/** Finds last active account or null if not exists */
	public Account findActiveAccount() {
		Cursor cursor = db.query(DBHelper.TABLE_ACCOUNTS, null,
				DBHelper.COLUMN_ACTIVE + " = ?", new String[] { "1" }, null,
				null, null);
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			String acc = cursor.getString(cursor
					.getColumnIndex(DBHelper.COLUMN_ACCOUNT_NAME));
			String pass = cursor.getString(cursor
					.getColumnIndex(DBHelper.COLUMN_PASSWORD));
			return new Account(acc, pass, 1);
		}
		return null;
	}

	/** Deletes account from all tables */
	public void deleteAccount(String account) {
		db.delete(DBHelper.TABLE_ACCOUNTS, DBHelper.COLUMN_ACCOUNT_NAME
				+ " = ?", new String[] { account });
	}

	/** Returns true if username and password matchs with database */
	public boolean loginAccount(String username, String password) {
		Cursor cursor = db.query(DBHelper.TABLE_ACCOUNTS, null,
				DBHelper.COLUMN_ACCOUNT_NAME + " = ?",
				new String[] { username }, null, null, null);
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			String acc = cursor.getString(cursor
					.getColumnIndex(DBHelper.COLUMN_ACCOUNT_NAME));
			String pass = cursor.getString(cursor
					.getColumnIndex(DBHelper.COLUMN_PASSWORD));
			if (acc.equals(username) && pass.equals(password)) {
				return true;
			}
		}
		return false;
	}

	/** updates totalDistance of username */
	public void updateTotalDistance(String username, float distance) {
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_TOTAL_DISTANCE, (int) distance);
		db.update(DBHelper.TABLE_ACCOUNTS, values, DBHelper.COLUMN_ACCOUNT_NAME
				+ "=?", new String[] { username });
	}

	/** updates totalTime of username */
	public void updateTotalTime(String username, long time) {
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_TOTAL_TIME, time);
		db.update(DBHelper.TABLE_ACCOUNTS, values, DBHelper.COLUMN_ACCOUNT_NAME
				+ "=?", new String[] { username });
	}

	/** updates maxSpeed of username */
	public void updateMaxSpeed(String username, float maxSpeed) {
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_MAX_SPEED, maxSpeed);
		db.update(DBHelper.TABLE_ACCOUNTS, values, DBHelper.COLUMN_ACCOUNT_NAME
				+ "=?", new String[] { username });
	}

	/** returns totalDistance of account */
	public float getTotalDistance(String username) {
		float distance = 0;
		Cursor cursor = db.query(DBHelper.TABLE_ACCOUNTS, null,
				DBHelper.COLUMN_ACCOUNT_NAME + " = ?",
				new String[] { username }, null, null, null);
		cursor.moveToFirst();
		int position = cursor.getColumnIndex(DBHelper.COLUMN_TOTAL_DISTANCE);
		if (cursor.isNull(position)) {
			return 0;
		}
		distance = cursor.getInt(position);
		return distance;
	}

	/** returns totalTime of account */
	public long getTotalTime(String username) {
		long time = 0;
		Cursor cursor = db.query(DBHelper.TABLE_ACCOUNTS, null,
				DBHelper.COLUMN_ACCOUNT_NAME + " = ?",
				new String[] { username }, null, null, null);
		cursor.moveToFirst();
		int position = cursor.getColumnIndex(DBHelper.COLUMN_TOTAL_TIME);
		if (cursor.isNull(position)) {
			return 0;
		}
		time = cursor.getLong(position);
		return time;
	}

	/** returns maxSpeed of account */
	public float getMaxSpeed(String username) {
		float speed = 0;
		Cursor cursor = db.query(DBHelper.TABLE_ACCOUNTS, null,
				DBHelper.COLUMN_ACCOUNT_NAME + " = ?",
				new String[] { username }, null, null, null);
		cursor.moveToFirst();
		int position = cursor.getColumnIndex(DBHelper.COLUMN_MAX_SPEED);
		if (cursor.isNull(position)) {
			return 0;
		}
		speed = cursor.getFloat(position);
		return speed;
	}

	/** Saves drive to database */
	public boolean insertDrive(String accName, Drive drive) {
		ContentValues driveValues = new ContentValues();
		long startDate = drive.getStartDate().getTime();
		driveValues.put(DBHelper.COLUMN_STARTDATE, startDate);
		driveValues.put(DBHelper.COLUMN_DISTANCE, drive.getDistance());
		driveValues.put(DBHelper.COLUMN_DURATION, drive.getDuration());
		driveValues.put(DBHelper.FOREIGNKEY_ACCOUNT, accName);
		driveValues.put(DBHelper.FOREIGNKEY_MODE, drive.getModeID());
		driveValues.put(DBHelper.FOREIGNKEY_VEHICLE, drive.getVehicleID());
		if (db.insert(DBHelper.TABLE_DRIVE, null, driveValues) == -1) {
			return false;
		}
		return insertTrackPoints(drive);
	}

	private boolean insertTrackPoints(Drive drive) {
		// TODO!
		return true;
	}

	public Drive[] getDrives(String account) {
		List<Drive> drives = new ArrayList<Drive>();
		Cursor cursor = getDriveCursor(account);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Drive drive = cursorToDrive(cursor);
			drives.add(drive);
			cursor.moveToNext();
		}
		cursor.close();
		return drives.toArray(new Drive[drives.size()]);
	}

	private Drive cursorToDrive(Cursor cursor) {
		long startTime = cursor.getLong(cursor
				.getColumnIndex(DBHelper.COLUMN_STARTDATE));
		Date startDate = new Date();
		startDate.setTime(startTime);
		Long duration = cursor.getLong(cursor
				.getColumnIndex(DBHelper.COLUMN_DURATION));
		Float distance = cursor.getFloat(cursor
				.getColumnIndex(DBHelper.COLUMN_DISTANCE));
		
		// TODO!! trackpoints
		return new Drive(startDate, duration, distance);
	}

	private Cursor getDriveCursor(String account) {
		Cursor results = db.query(DBHelper.TABLE_DRIVE, null,
				DBHelper.FOREIGNKEY_ACCOUNT + " = ?", new String[] { account },
				null, null, null);
		return results;
	}
	
	public void deleteDrive(String account, Date date) {
		db.delete(DBHelper.TABLE_DRIVE, 
				DBHelper.FOREIGNKEY_ACCOUNT	+ " = ? AND "+
		DBHelper.COLUMN_STARTDATE +" = ?", new String[] { account,String.valueOf(date.getTime())});
	}

}
