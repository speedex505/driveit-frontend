package com.sabatsoft.driveit.activity;

import android.os.Bundle;
import com.sabatsoft.driveit.R;

public class SettingsActivity extends android.preference.PreferenceActivity {
	
    @SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.settings);
    }    
}