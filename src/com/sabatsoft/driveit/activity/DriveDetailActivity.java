package com.sabatsoft.driveit.activity;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.engine.Drive;
import com.sabatsoft.driveit.engine.DriveItEngine;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class DriveDetailActivity extends Activity {

	Drive drive;	
	TextView test;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DriveItEngine.getTmpDrive().clearTrackPoints();
		drive = DriveItEngine.getTmpDrive();
		drive.loadTrackPoints(this);
		setContentView(R.layout.activity_drive_detail);
		test=(TextView)findViewById(R.id.test);
		test.setText("TrackPoints: "+drive.getTrackPoints().length);
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.drive_detail, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
