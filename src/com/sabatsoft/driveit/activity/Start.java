package com.sabatsoft.driveit.activity;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.engine.DriveItEngine;

public class Start extends Activity {
AccountManager acc;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dstart);
		TextView title = (TextView)findViewById(R.id.start_tittle);
		Typeface tp = Typeface.createFromAsset(getAssets(), "fonts/321impact.ttf");
		title.setTypeface(tp);
		DriveItEngine.setCurrentAccount(null);
		new Handler().postDelayed(new Thread(){
			@Override
			public void run(){
				Intent accountManager= new Intent(Start.this,AccountManagerActivity.class);
				Start.this.startActivity(accountManager);
				Start.this.finish();
			}
		},3000);
	}
}
