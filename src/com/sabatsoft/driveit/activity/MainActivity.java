package com.sabatsoft.driveit.activity;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.database.DBConnector;
import com.sabatsoft.driveit.engine.Account;
import com.sabatsoft.driveit.engine.DriveItEngine;
import com.sabatsoft.driveit.fragment.*;
import com.sabatsoft.driveit.service.LocatorService;
import com.sabatsoft.driveit.service.LocatorService.LocatorBinder;


public class MainActivity extends FragmentActivity implements TabListener {

	protected PowerManager.WakeLock mWakeLock;
	private MenuItem settings;
	private MenuItem logout;
	
	private static final int SETTINGS =1;
	private static final int LOGOUT =2;
	
	public static ViewPager mViewPager;
	public static MyPagerAdapter mAdapter;
	private LocatorService locatorService;
	private TextView gpsStatus;
	
	public static ActionBar actionBar;
		
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		loadUserData();
        setContentView(R.layout.mainactivity);
        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.DISPLAY_SHOW_HOME);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#383838")));
        actionBar.setDisplayShowTitleEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);
        
        gpsStatus = (TextView) findViewById(R.id.gps_status);
        gpsStatus.setText("Searching for GPS signal...");
        mViewPager = (ViewPager) findViewById(R.id.mainactivity_pager);
	    mAdapter = new MyPagerAdapter(getSupportFragmentManager());
	    mViewPager.setAdapter(mAdapter);
	    //mViewPager.setOffscreenPageLimit(3);
	    mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
	    	@Override
	        public void onPageSelected(int position) {
	    		actionBar.setSelectedNavigationItem(position);
	        }
	    });
	    for (int i = 0; i < mAdapter.getCount(); i++) {
	    	actionBar.addTab(
	    			actionBar.newTab()
	                .setText(mAdapter.getPageTitle(i))
	                .setTabListener(this));
	    }
		Intent intent = new Intent(this, LocatorService.class);
		intent.putExtra("totalDistance", DriveItEngine.getAccount().getTotalDistance());
		intent.putExtra("totalTime", DriveItEngine.getAccount().getTotalTime());
	    bindService(intent, mConnection,Context.BIND_AUTO_CREATE);
		LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("LocatorService"));
    }
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}
	
	
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		saveUserData();
	    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);		// Enables power-saving
	}

	@Override
	protected void onResume() {
		turnGPSOn();
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);	    // Disables power-saving
	    super.onResume();
	}

	@Override
	protected void onDestroy() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
		unbindService(mConnection);
		locatorService=null;
		super.onDestroy();
	}	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        settings = menu.add(0, SETTINGS, 0, "Settings");
        settings.setIcon(android.R.drawable.ic_menu_preferences);
        //settings.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        logout = menu.add(0, LOGOUT,0,"Logout");
        logout.setIcon(android.R.drawable.ic_notification_clear_all);
        //logout.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
	public void onBackPressed() {
		new AlertDialog.Builder(this)
	    .setTitle("Exit")
	    .setMessage("Do you really want to exit?")
	    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        	finish(); // Exit app
	        }
	    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            // Do nothing
	        }
	    }).show();

	}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case LOGOUT:
        	saveUserData();
        	DBConnector db = new DBConnector(this);
        	db.open();
        	db.refreshActiveAccounts(".");
        	db.close();
            Intent intent = new Intent(this,AccountManagerActivity.class);
            startActivity(intent);
        	DriveItEngine.setCurrentAccount(null);
            finish();
            return true;
        case SETTINGS:
        	startActivity(new Intent(this,SettingsActivity.class));
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private void turnGPSOn(){
	    String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	    if(!provider.contains("gps")){ //if gps is not enabled
	    	showGPSAlert();
	    }
	}
	
    public void showGPSAlert(){
    	final Context mContext = this;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });
        // On pressing Cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });
        alertDialog.show();
    }
	private void saveUserData() {
		Account acc = DriveItEngine.getAccount();
		if(acc==null){
			return;
		}
		DBConnector db = new DBConnector(this);
		db.open();
		db.updateTotalDistance(acc.getName(),acc.getTotalDistance());
		db.updateTotalTime(acc.getName(), acc.getTotalTime());
		db.updateMaxSpeed(acc.getName(), acc.getMaxSpeed());
		db.close();
	}
	
	private void loadUserData(){
        float totalDistance;
        long totalTime;
        float maxSpeed;
        Account acc=DriveItEngine.getAccount();
        DBConnector db = new DBConnector(this);
		db.open();
		totalDistance=db.getTotalDistance(acc.getName());
		totalTime=db.getTotalTime(acc.getName());
		maxSpeed=db.getMaxSpeed(acc.getName());
		db.close();
		acc.setTotalDistance(totalDistance);
		acc.setTotalTime(totalTime);
		acc.updateMaxSpeed(maxSpeed);
	}
 	private class MyPagerAdapter extends FragmentStatePagerAdapter{
 		
 		ArrayList<Fragment> fragments = new ArrayList<Fragment>();
 		
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments.add(new TachometerFragment());
            fragments.add(new AccountInfoFragment());
            fragments.add(new NewDriveFragment());
            fragments.add(new DrivesFragment());
        }  
        @Override
        public Fragment getItem(int i) {
        	return fragments.get(i);
        }
		@Override
		public int getCount() {
			return fragments.size();
		}
        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
            	case 0:
            		return "Speedometer";
            	case 1:
            		return "Info";
            	case 2:
            		return "New drive";
            	case 3:
            		return "Drives";
            	default:
            		return "Insert tittle";
            }
        }
        
        public void changeFragmentAdapter(Fragment fragment, int position){
        	fragments.remove(position);
        	fragments.add(position, fragment);
        }
 	}
	public void changeFragment(Fragment fragment, int position){
		actionBar.removeAllTabs();
		mAdapter.changeFragmentAdapter(fragment,position);
	    for (int i = 0; i < mAdapter.getCount(); i++) {
	    	actionBar.addTab(
	    			actionBar.newTab()
	                .setText(mAdapter.getPageTitle(i))
	                .setTabListener(this));
	    }
	    mViewPager.setCurrentItem(position);
	}
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			LocatorBinder binder = (LocatorBinder) service;
			locatorService = binder.getService();
			locatorService.startGPS();
		}

		public void onServiceDisconnected(ComponentName className) {
			if (locatorService != null) {
				locatorService.stopGPS();
			}
			locatorService = null;
		}
	};
	private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int message = intent.getIntExtra("message", LocatorService.ERROR);
			switch(message){
			case LocatorService.LOW_ACCURACY:
				gpsStatus.setText("Low or none signal");
				break;
			case LocatorService.NO_SIGNAL:
				gpsStatus.setText("Searching for GPS signal...");
				break;
			case LocatorService.OK:
				gpsStatus.setText("Good signal");
				break;
			default:
				gpsStatus.setText("Error");
			}
		}
	};

}
