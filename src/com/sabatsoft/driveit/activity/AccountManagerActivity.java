package com.sabatsoft.driveit.activity;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;

import com.sabatsoft.driveit.R;
import com.sabatsoft.driveit.database.DBConnector;
import com.sabatsoft.driveit.engine.Account;
import com.sabatsoft.driveit.engine.DriveItEngine;
import com.sabatsoft.driveit.fragment.AccountLoginFragment;
import com.sabatsoft.driveit.fragment.AccountsListFragment;


public class AccountManagerActivity extends FragmentActivity implements TabListener{
	
	private ViewPager mViewPager;
	private MyPagerAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DriveItEngine.setStatus(DriveItEngine.BEFORE_LOG);
	    findAllreadyLogon();
	    setContentView(R.layout.accountmanager);
	    final ActionBar actionBar = getActionBar();
	    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
	        
	    mViewPager = (ViewPager) findViewById(R.id.accountmanager_pager);
	    mAdapter = new MyPagerAdapter(getSupportFragmentManager());
	    mViewPager.setAdapter(mAdapter);
	    mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
	    	@Override
	        public void onPageSelected(int position) {
	    		actionBar.setSelectedNavigationItem(position);
	        }
	    });
	    for (int i = 0; i < mAdapter.getCount(); i++) {
	    	actionBar.addTab(
	    			actionBar.newTab()
	                .setText(mAdapter.getPageTitle(i))
	                .setTabListener(this));
	    }
	}
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	} 
	 
	 
 	private void findAllreadyLogon() {
		Account acc;
		DBConnector db = new DBConnector(this);
		db.open();
		acc= db.findActiveAccount();
		if(acc!=null){
			DriveItEngine.setCurrentAccount(acc);
			db.refreshActiveAccounts(acc.getName());
			db.close();
			DriveItEngine.setStatus(DriveItEngine.BEFORE_START);
			Intent test= new Intent(this,MainActivity.class);
			this.startActivity(test);
			this.finish();	
		}
		db.close();		
	}
 	
 	private class MyPagerAdapter extends FragmentPagerAdapter{
 		
 		ArrayList<Fragment> fragments = new ArrayList<Fragment>();
 		
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments.add(new AccountLoginFragment());
            fragments.add(new AccountsListFragment());
        }  

        @Override
        public Fragment getItem(int i) {
        	return fragments.get(i);
        }

		@Override
		public int getCount() {
			return fragments.size();
		}
        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
            	case 0:
            		return "Sign in / up";
            	case 1:
            		return "Accounts list";
            	default:
            		return "Insert tittle";
            }
        }
 	}


}
