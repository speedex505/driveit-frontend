package com.sabatsoft.driveit.activity;

import com.sabatsoft.driveit.R;

import com.sabatsoft.driveit.engine.DriveItEngine;
import com.sabatsoft.driveit.service.LocatorService;
import com.sabatsoft.driveit.service.LocatorService.LocatorBinder;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.TextView;

public class DriveActivity extends FragmentActivity{
	private TextView gpsStatus;
	private LocatorService locatorService;
	
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.driveactivity);
	        gpsStatus = (TextView) findViewById(R.id.gps_status_driveActivity);
	        gpsStatus.setText("Searching for GPS signal...");
			Intent locatorIntent = new Intent(this, LocatorService.class);
			locatorIntent.putExtra("totalDistance", DriveItEngine.getAccount().getTotalDistance());
			locatorIntent.putExtra("totalTime", DriveItEngine.getAccount().getTotalTime());
		    bindService(locatorIntent, locatorConnection,Context.BIND_AUTO_CREATE);
		    LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("LocatorService"));
	 }
	 
		private ServiceConnection locatorConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName className, IBinder service) {
				LocatorBinder binder = (LocatorBinder) service;
				locatorService = binder.getService();
				locatorService.startGPS();
				locatorService.resetDistance();
			}

			public void onServiceDisconnected(ComponentName className) {
				if (locatorService != null) {
					locatorService.stopGPS();
				}
				locatorService = null;
			}
		};
		private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				int message = intent.getIntExtra("message", LocatorService.ERROR);
				switch(message){
				case LocatorService.LOW_ACCURACY:
					gpsStatus.setText("Low or none signal");
					break;
				case LocatorService.NO_SIGNAL:
					gpsStatus.setText("Searching for GPS signal...");
					break;
				case LocatorService.OK:
					gpsStatus.setText("Good signal");
					break;
				default:
					gpsStatus.setText("Error");
				}
			}
		};

		@Override
		protected void onDestroy() {
			unbindService(locatorConnection);
			LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
			super.onDestroy();
		}
		public void resetDistance(){
			locatorService.resetDistance();
		}
}
